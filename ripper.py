import subprocess

def do_handbrake(device,path,name,log):
	subprocess.call("/Applications/HandbrakeCLI -i /dev/"+device+" -o /Users/ripperroo/Movies/"+name+" -e x264 >"+tmp_file,shell=True)
	subprocess.call("rm " + tmp_file,shell=True)
	log.write("\t[!] Handbrake Complete!\n")

def eject_device(device,log):
	subprocess.call(["diskutil","eject","/dev/"+device])
	log.write("[!] Ejecting: /dev/"+device+"\n")

def get_file_name(path):
	name = ""
	try:
		f = open(path+"disc.id","r")
		data = f.read()
		name = data.split("name=")[1].split("\n")[0]
		f.close()
	except:
		name = path.split("/")[-1]
	return name

log = open("/Users/ripperroo/ripper/logger","a+")
proc = subprocess.Popen(['diskutil','activity'],stdout=subprocess.PIPE)
disk_activity = ""
while "\'disk" not in disk_activity:
  disk_activity = proc.stdout.readline()
print disk_activity
proc.terminate()
log.write("[+] Disk inserted: "+disk_activity)
device = disk_activity.split("('")[1].split("'")[0]
path = disk_activity.split("file://")[1].split("'")[0]
name = get_file_name(path).replace(" ","_").replace("\r","")+".mp4"

log.write("\t[*] Disk Device: /dev/"+device+"\n")
log.write("\t[*] Disk Path: "+path+"\n")
log.write("\t[*] Video Name: "+name+"\n")
tmp_file = "/tmp/"+name
log.write("\t[!] Running Handbrake -> "+tmp_file+"!\n")
log.close()
log = open("/Users/ripperroo/ripper/logger","a+")
do_handbrake(device,path,name,log)
eject_device(device,log)
